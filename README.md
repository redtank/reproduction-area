- [relative link](2016-05-11_15-40-00.jpg)
- [explicit relative link](./2016-05-11_15-40-00.jpg)
- [absolute link](/2016-05-11_15-40-00.jpg)

```markdown
- [relative link](2016-05-11_15-40-00.jpg)
- [explicit relative link](./2016-05-11_15-40-00.jpg)
- [absolute link](/2016-05-11_15-40-00.jpg)
```
